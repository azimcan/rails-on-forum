Rails.application.routes.draw do
  resource :sessions, only: [:new, :create, :destroy] 
  get '/oturum_ac', to: 'sessions#new', as: :login
  delete '/oturum_kapat', to: 'sessions#destroy', as: :logout


  resources :users, except: :index
  get '/users/new', to: redirect('/kayit')
  get '/kayit', to: 'users#new', as: :register
  get '/:id', to: 'users#show', as: :profile
  get '/:id/edit', to: 'users#edit', as: :edit_profile  
end
