module ApplicationHelper
	def flash_class(type)
		{ notice: 'info', alert: 'warning', error: 'danger' }[type.to_sym]		
	end
end
