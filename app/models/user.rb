class User < ApplicationRecord
	has_secure_password

	validates :username,
		exclusion: { in: ['oturum_ac'] },
		presence: true,
		uniqueness: { case_sensitive: false },
		length: { in: 4..12 },
		format: { with: /\A[a-zA-Z][a-zA-Z0-9_-]*\Z/ }

	validates :first_name,
		presence: true

	validates :last_name,
		presence: true

	validates :email,
		presence: true,
		uniqueness: { case_sensitive: false },
		email: true

=begin	
		validates :password, 
		presence: true,
		length: { minimum: 6 }
=end

	def to_param
		username		
	end
	
	def name
		"#{first_name} #{last_name}"
	end

	def user_detail
		"#{first_name} #{last_name} -- #{username} -- #{email} -- #{password_digest}"
	end

	def avatar_url
		hash_value = Digest::MD5.hexdigest(email.downcase)
		'http://gravatar.com/avatar/#{hash_value}?s=160'
	end

end
