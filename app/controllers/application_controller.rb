class ApplicationController < ActionController::Base
	helper_method :signed_in?, :current_user, :login
	private

	def login(user)
		session[:user_id] = user.id
	end

	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	def signed_in?
		current_user		
	end
end
